package Reader;

import java.io.*;

public class File04 {

  public static void main(String[] args) throws Exception {
    BufferedReader r  = 
	       new BufferedReader(new FileReader("E:\\BSC\\Advanced Software Engineering\\Lecture 05\\Task 5\\data/wolf-fox.txt"));

	   r.lines()
	    .map(l -> l.toUpperCase())
	    .forEach(l -> System.out.println(l));
	    
	   r.close();
  }

}
