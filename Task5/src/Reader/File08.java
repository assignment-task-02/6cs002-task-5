package Reader;

import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

public class File08 {

	public static void main(String[] args) throws Exception {
	  BufferedReader r  = 
	      new BufferedReader(new FileReader("E:\\BSC\\Advanced Software Engineering\\Lecture 05\\Task 5\\data/wolf-fox.txt"));

	  List<String> l = r.lines().collect(Collectors.toList());
	    
	  for(String line: l){
	    System.out.println(line);
	  }
	    
	  r.close();
	}
}

