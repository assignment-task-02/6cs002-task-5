package Reader;

import java.io.*;
import java.util.Optional;

public class File07 {

	public static void main(String[] args) throws Exception {
	  BufferedReader r  = 
	    new BufferedReader(new FileReader("E:\\BSC\\Advanced Software Engineering\\Lecture 05\\Task 5\\data/wolf-fox.txt"));

	  Optional <String >result = 
	    r.lines()
	     .reduce((left, right) -> left.concat(" ".concat(right)));
	    
	  if(result.isPresent())
	    System.out.println("Result is: " + result.get());
	  else
	    System.out.println("Result not present");
	  r.close();
	}
}

