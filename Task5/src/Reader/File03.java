package Reader;

import java.io.*;

public class File03 {

  public static void main(String[] args) throws Exception {
	   BufferedReader r = new BufferedReader(new FileReader("E:\\BSC\\Advanced Software Engineering\\Lecture 05\\Task 5\\data/wolf-fox.txt"));

	   r.lines().filter(l -> l.contains("his"))
	       .forEach(l -> System.out.println(l));

	   r.close();
	}

}

