package Cricket;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Cricket8 {
	
    public static void main(String[] args) {
        List<Team> table = Arrays.asList(
                new Team(8, "South Africa", 24, 9, 13, 2, 2, 98, "-0.077", -2),
                new Team(2, "England", 24, 15, 8, 0, 1, 155, "+0.976", 0),
                new Team(1, "New Zealand", 24, 16, 5, 0, 3, 175, "+0.914", 0),
                new Team(5, "Afghanistan", 24, 14, 3, 0, 1, 145, "+0.573", 0),
                new Team(7, "Pakistan", 24, 13, 8, 0, 0, 130, "+0.108", 0),
                new Team(3, "Bangladesh", 24, 15, 8, 0, 1, 155, "+0.220", 0),
                new Team(10, "Sri Lanka", 24, 7, 14, 0, 3, 81, "-0.369", -4),
                new Team(4, "Australia", 24, 15, 9, 0, 0, 150, "+0.785", 0),
                new Team(11, "Ireland", 24, 6, 15, 0, 3, 73, "-0.357", -2),
                new Team(9, "West Indies", 24, 9, 15, 0, 0, 88, "-0.738", -2),
                new Team(6, "India", 24, 13, 6, 0, 2, 139, "+0.782", -1),
                new Team(12, "Zimbabwe", 24, 6, 17, 0, 1, 65, "-0.952", 0));
        
        // Computationally expensive task: Calculating the square of points for each team
        System.out.println("Computing squares of points...");

        // Sequential stream
        long startTimeSeq = System.currentTimeMillis();
        List<Integer> pointsSquaresSeq = table.stream()
                .map(team -> team.getPoints() * team.getPoints())
                .collect(Collectors.toList());
        long endTimeSeq = System.currentTimeMillis();
        long executionTimeSeq = endTimeSeq - startTimeSeq;

        System.out.println("Sequential Stream Execution Time: " + executionTimeSeq + " milliseconds");
        System.out.println("Points Squares (Sequential): " + pointsSquaresSeq);

        // Parallel stream
        long startTimeParallel = System.currentTimeMillis();
        List<Integer> pointsSquaresParallel = table.parallelStream()
                .map(team -> team.getPoints() * team.getPoints())
                .collect(Collectors.toList());
        long endTimeParallel = System.currentTimeMillis();
        long executionTimeParallel = endTimeParallel - startTimeParallel;

        System.out.println("Parallel Stream Execution Time: " + executionTimeParallel + " milliseconds");
        System.out.println("Points Squares (Parallel): " + pointsSquaresParallel);
    }
}
