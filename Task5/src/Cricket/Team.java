package Cricket;

public class Team implements Comparable<Team> {
	
	    private int rank;
	    private String name;
	    private int matches;
	    private int won;
	    private int lost;
	    private int tied;
	    private int noResult;
	    private int points;
	    private String nrr;
	    private int penaltyOvers;

	    public Team(int rank, String name, int matches, int won, int lost, int tied, int noResult,
	                int points, String nrr, int penaltyOvers) {
	        this.rank = rank;
	        this.name = name;
	        this.matches = matches;
	        this.won = won;
	        this.lost = lost;
	        this.tied = tied;
	        this.noResult = noResult;
	        this.points = points;
	        this.nrr = nrr;
	        this.penaltyOvers = penaltyOvers;
	    }

	    // Getters and setters
	    
	    public String toString() {
	        return String.format("%-3d%-20s%-8d%-8d%-8d%10d", rank, name,matches,won,lost, points);
	    }

	    public int getRank() {
	        return rank;
	    }

	    public void setRank(int rank) {
	        this.rank = rank;
	    }

	    public String getName() {
	        return name;
	    }

	    public void setName(String name) {
	        this.name = name;
	    }

	    public int getMatches() {
	        return matches;
	    }

	    public void setMatches(int matches) {
	        this.matches = matches;
	    }

	    public int getWon() {
	        return won;
	    }

	    public void setWon(int won) {
	        this.won = won;
	    }

	    public int getLost() {
	        return lost;
	    }

	    public void setLost(int lost) {
	        this.lost = lost;
	    }

	    public int getTied() {
	        return tied;
	    }

	    public void setTied(int tied) {
	        this.tied = tied;
	    }

	    public int getNoResult() {
	        return noResult;
	    }

	    public void setNoResult(int noResult) {
	        this.noResult = noResult;
	    }

	    public int getPoints() {
	        return points;
	    }

	    public void setPoints(int points) {
	        this.points = points;
	    }

	    public String getNRR() {
	        return nrr;
	    }

	    public void setNRR(String nrr) {
	        this.nrr = nrr;
	    }

	    public int getPenaltyOvers() {
	        return penaltyOvers;
	    }

	    public void setPenaltyOvers(int penaltyOvers) {
	        this.penaltyOvers = penaltyOvers;
	    }
		  
	    public int compareTo(Team c) {
	        return ((Integer) won).compareTo(c.lost);
	    }
}
