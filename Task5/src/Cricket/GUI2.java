package Cricket;

import javax.swing.*;
import java.awt.*;

public class GUI2 {

	  public static void main(String[] args) {
	    JFrame f = new JFrame();
	    JButton b = new JButton("Press Me");
	    f.setLayout(new BorderLayout());
	    f.add(b, BorderLayout.CENTER);
	    b.addActionListener(
	        event -> System.out.println("Button says: " + 
	                   event.getActionCommand()));
	    f.pack();
	    f.setVisible(true);
	  }

	}
