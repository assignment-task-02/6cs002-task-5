package Cricket;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;

public class GUI1 {

    public static void main(String[] args) {
        JFrame f = new JFrame();
        JButton b = new JButton("Press Me");
        JTextArea textArea = new JTextArea();
        JScrollPane scrollPane = new JScrollPane(textArea);

        f.setLayout(new BorderLayout());
        f.add(scrollPane, BorderLayout.CENTER);
        f.add(b, BorderLayout.SOUTH);

        b.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                List<Team> table = Arrays.asList(
                        new Team(8, "South Africa", 24, 9, 13, 2, 2, 98, "-0.077", -2),
                        new Team(2, "England", 24, 15, 8, 0, 1, 155, "+0.976", 0),
                        new Team(1, "New Zealand", 24, 16, 5, 0, 3, 175, "+0.914", 0),
                        new Team(5, "Afghanistan", 24, 14, 3, 0, 1, 145, "+0.573", 0),
                        new Team(7, "Pakistan", 24, 13, 8, 0, 0, 130, "+0.108", 0),
                        new Team(3, "Bangladesh", 24, 15, 8, 0, 1, 155, "+0.220", 0),
                        new Team(10, "Sri Lanka", 24, 7, 14, 0, 3, 81, "-0.369", -4),
                        new Team(4, "Australia", 24, 15, 9, 0, 0, 150, "+0.785", 0),
                        new Team(11, "Ireland", 24, 6, 15, 0, 3, 73, "-0.357", -2),
                        new Team(9, "West Indies", 24, 9, 15, 0, 0, 88, "-0.738", -2),
                        new Team(6, "India", 24, 13, 6, 0, 2, 139, "+0.782", -1),
                        new Team(12, "Zimbabwe", 24, 6, 17, 0, 1, 65, "-0.952", 0));

                StringBuilder sb = new StringBuilder();
                table.forEach(x -> sb.append(x).append("\n"));
                textArea.setText(sb.toString());
            }
        });

        textArea.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));

        f.pack();
        f.setVisible(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}
