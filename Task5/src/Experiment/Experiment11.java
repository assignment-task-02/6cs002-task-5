package Experiment;

import java.util.Arrays;
import java.util.List;

public class Experiment11 {

	public void run() {
	  String[] n1 = { "Apple", "Mango", "Pineapple","Grapes" };

	  List<String> n2 = Arrays.asList(n1);

	  n2.stream().map(fruit -> fruit.replaceAll("e", ""))
	      .forEach(fruit -> System.out.println(fruit));

	}

	public static void main(String[] args) {
	  new Experiment11().run();
	}
}

