package Experiment;

import java.util.Arrays;
import java.util.List;

public class Experiment08 {

	public void run() {
	   String[] n1 = { "Apple", "Mango", "Pineapple","Grapes" };

	   List<String> n2 = Arrays.asList(n1);  

	   System.out.println("Serial Fruits\n---------");
	   n2.stream().forEach(Fruit -> System.out.println(Fruit));

	   System.out.println("\nParallel Fruits\n---------");
	   n2.parallelStream().forEach(Fruit -> System.out.println(Fruit));
	 }

	 public static void main(String[] args) {
	  new Experiment08().run();
	}
}

