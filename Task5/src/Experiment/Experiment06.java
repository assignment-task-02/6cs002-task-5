package Experiment;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.List;

public class Experiment06 {
	 class StringPrintConsumer implements Consumer<String>{
	   public void accept(String str) {
      System.out.println(str);     
	   } 
	 }
	  
	 public void run() {
	   String[] n1 = { "Apple", "Mango", "Pineapple","Grapes" };

	   List<String> n2 = Arrays.asList(n1);  

      n2.forEach(new StringPrintConsumer());
	 }
	  
	  public static void main(String[] args) {
	   new Experiment06().run();
	 }
}

