package Experiment;

import java.util.Arrays;
import java.util.function.Function;
import java.util.List;

public class Experiment12 {
	class ERemover implements Function<String, String> {
	  public String apply(String fruit) {
	    return fruit.replaceAll("e", "");
	  }
	}
	  
	public void run() {
	  String[] n1 = { "Apple", "Mango", "Pineapple","Grapes" };

	  List<String> n2 = Arrays.asList(n1);

	  n2.stream().map(new ERemover())
	      .forEach(fruit -> System.out.println(fruit));

	}

	public static void main(String[] args) {
	  new Experiment12().run();
	}
}

