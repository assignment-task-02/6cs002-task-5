package Experiment;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Experiment04 {
    public static void main(String[] args) {
        String[] n1 = { "Apple", "Mango", "Pineapple","Grapes" };

        List<String> n2 = new ArrayList<>(Arrays.asList(n1));
        System.out.println(n2.getClass());

        for (Method m : n2.getClass().getDeclaredMethods()) {
            System.out.println(m.getName());
        }

        n2.add("Strawberry");
        System.out.println(n2);
    }
}


