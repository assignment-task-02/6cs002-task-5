package Experiment;

import java.util.Arrays;
import java.util.List;

public class Experiment09 {

	public void run() {
	  String[] n1 = { "Apple", "Mango", "Pineapple","Grapes" };

	  List<String> n2 = Arrays.asList(n1);

	  n2.stream().filter(Fruit -> Fruit.contains("e"))
	      .forEach(Fruit -> System.out.println(Fruit));

	}

	public static void main(String[] args) {
	  new Experiment09().run();
	}
}

