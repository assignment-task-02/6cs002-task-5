package Experiment;

import java.util.Arrays;
import java.util.List;

public class Experiment05 {
	 public static void main(String[] args) {
	   String[] n1 = { "Apple", "Mango", "Pineapple","Grapes" };

	   List<String> n2 = Arrays.asList(n1);  

	   n2.forEach(Fruit -> System.out.println(Fruit));
	}
}

