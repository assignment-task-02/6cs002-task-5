package Experiment;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class Experiment10 {
	class EFilter implements Predicate<String>{
	  public boolean test(String fruit) {
	    return fruit.contains("e");
	  }    
	}

	public void run() {
	  String[] n1 = { "Apple", "Mango", "Pineapple","Grapes"};

	  List<String> n2 = Arrays.asList(n1);

	  n2.stream().filter(new EFilter())
	      .forEach(Fruit -> System.out.println(Fruit));

	}

	public static void main(String[] args) {
	  new Experiment10().run();
	}
}

