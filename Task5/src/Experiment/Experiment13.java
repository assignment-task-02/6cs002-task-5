package Experiment;

import java.util.function.Supplier;

public class Experiment13 {
	class NameSupplier implements Supplier<String>{
	  String[] fruit = { "Apple", "Mango", "Pineapple","Grapes" };
	  int nextIndex = 0;
	  public String get() {
	    if(nextIndex< fruit.length) {
	      return fruit[nextIndex++];
	    }
	    return null;
	  }  
	}
	  
	 public void run() {
	  NameSupplier ns = new NameSupplier();

	  System.out.println(ns.get());
	  System.out.println(ns.get());
	  System.out.println(ns.get());
	  System.out.println(ns.get());
	}

	public static void main(String[] args) {
	  new Experiment13().run();
	}
}

